//
//  iCloudDocument.m
//  iCloud example
//
//  Created by Simone Kalb on 2/20/17.
//  Copyright © 2017 Simone Kalb. All rights reserved.
//

#import "iCloudDocument.h"

NSString *const documentNameKey = @"name";
NSString *const documentImageKey = @"image";

@interface iCloudDocument ()
@property (nonatomic,strong) NSMutableDictionary *document;
@end

@implementation iCloudDocument

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName
                   error:(NSError **)outError {
    
    if ([contents length] > 0) {
        
        self.document = (NSMutableDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:contents];
    } else {
        
        self.document = [NSMutableDictionary dictionary];
    }
    
    return YES;
}

- (id)contentsForType:(NSString *)typeName error:(NSError **)outError {
    
    if ([self.document count] == 0) {
        self.document = [NSMutableDictionary dictionary];
    }
    
    return [NSKeyedArchiver archivedDataWithRootObject:self.document];
}

-(void)setImage:(UIImage *)image {
    
    self.document[documentImageKey] = image;
}

-(void)setName:(NSString *)name {
    
    self.document[documentNameKey] = name;
}

@end
