//
//  iCloudDocument.h
//  iCloud example
//
//  Created by Simone Kalb on 2/20/17.
//  Copyright © 2017 Simone Kalb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iCloudDocument : UIDocument
- (void)setImage:(UIImage *)image;
- (void)setName:(NSString *)name;
@end
