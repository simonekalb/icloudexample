//
//  ViewController.m
//  iCloud example
//
//  Created by Simone Kalb on 2/20/17.
//  Copyright © 2017 Simone Kalb. All rights reserved.
//

#import "ViewController.h"
#import "Manager.h"

@interface ViewController ()
@property (nonatomic, strong) Manager *manager;

- (IBAction)cropButtonPressed:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager = [Manager new];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)cropButtonPressed:(id)sender {
    [self.manager addDocument];
}
@end
