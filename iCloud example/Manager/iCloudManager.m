//
//  iCloudManager.m
//  iCloud example
//
//  Created by Simone Kalb on 2/20/17.
//  Copyright © 2017 Simone Kalb. All rights reserved.
//

#import "iCloudManager.h"
#import "iCloudDocument.h"

@interface iCloudManager()
@property(nonatomic, strong) NSURL *iCloudURL;
@end

@implementation iCloudManager

- (void) saveiCloudDocumentWithCompletionBlock:(void(^)(BOOL))completionBlock {
    
    if (self.iCloudURL) {
        
        NSURL *documentsURL = [self.iCloudURL URLByAppendingPathComponent:@"Documents"];
        
        NSString *iCloudFileName = [[NSUUID UUID] UUIDString]; // Create a unique file name
        NSURL *croppingURL = [documentsURL URLByAppendingPathComponent:iCloudFileName];
        
        iCloudDocument *cloudDocument = [[iCloudDocument alloc] initWithFileURL:croppingURL];
        [cloudDocument setImage:[UIImage imageNamed:@"logo"]];
        [cloudDocument setName:iCloudFileName];
        
        [cloudDocument saveToURL:croppingURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            
            if (!success) {
                
                NSLog(@"Save failed.");
                return completionBlock(NO);
                
            } else {
                
                NSLog(@"Save succeeded.");
                return completionBlock(YES);
            }
        }];
    }
}

- (NSURL*) iCloudURL {
    
    if ([[NSFileManager defaultManager] ubiquityIdentityToken]) {
        
        return _iCloudURL;
    }
    
    return nil;
}

@end
