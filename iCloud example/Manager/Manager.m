//
//  Manager.m
//  iCloud example
//
//  Created by Simone Kalb on 2/20/17.
//  Copyright © 2017 Simone Kalb. All rights reserved.
//

#import "Manager.h"
#import "iCloudManager.h"

const char *serialQueueIdentifier = "icloud.example.serial.queue";

@interface Manager()
@property (nonatomic, strong) NSMutableArray *documents;
@property (nonatomic) dispatch_queue_t serialQueue;
@property (nonatomic, strong) iCloudManager *iCloudManager;
@end


@implementation Manager

- (instancetype)init {
    
    self = [super init];
    if(self) {
        self.serialQueue = dispatch_queue_create(serialQueueIdentifier, DISPATCH_QUEUE_SERIAL);
        self.iCloudManager = [[iCloudManager alloc] init];
        self.documents = [NSMutableArray new];

    }
    return self;
}

-(void) addDocumentWithCompletionBlock:(void(^)(BOOL))completionBlock {
    
    dispatch_async(self.serialQueue, ^{
        
        [self.iCloudManager saveiCloudDocumentWithCompletionBlock:^(BOOL completed) {
            
            if(!completed) {
                
                return completionBlock(NO);
            }
            
            return completionBlock(YES);
        }];
    });
}

@end
