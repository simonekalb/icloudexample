//
//  Manager.h
//  iCloud example
//
//  Created by Simone Kalb on 2/20/17.
//  Copyright © 2017 Simone Kalb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Manager : NSObject

-(void) addDocumentWithCompletionBlock:(void(^)(BOOL))completionBlock;

@end
